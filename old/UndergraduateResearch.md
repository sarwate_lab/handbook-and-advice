# Skills/goals for REU Students

* Research skills
  * how to read a paper
  * doing an exhaustive literature search
  * time management and planning
  * prep for research meetings / self-assessment
  * organization of notes
  * how to think about experiments / simulations
* Communication skills
  * assessing the audience
  * "chalk" talks
  * poster design
  * giving a talk with slides
  * doing a technical writeup / anatomy of a paper
  * "pitching" a new project
* Software:
  * Python / ipython
  * visualization / graphing
  * git
  * LaTeX
  * bibliographic management

