## Research Inputs

It's important to gain both breadth and depth in grad school. **write more stuff here**

* Go to talks
* Read papers
* Read non-papers which are also technical
* Look at code

### Sign up for seminars and mailing lists

You should plan to go to lots of talks! At first, the talks will be confusing because you don't know as much about research. But later on you will start to understand more and more. There are many benefits of going to talks on topics that are not **only about your research**:

* Being aware of what other work is out there is part of general scientific literacy: if you go to an interview and have only learned about some tiny narrow thing, the interviewer will think that you are too specialized and can't learn new things.
* The greatest asset to a researcher is curiosity. If something about a talk sounds interesting, then go to it! You will always learn something and any day you learn something new is a good day.
* Ideas from other fields can help you with your problem: maybe there are connections you didn't know existed, or a whole area of research which is interesting and related to what you are working on.

At Rutgers, here are some seminars of interest:

* [SIP Seminar](http://www.inspirelab.us/seminars/): email [ECE_SIP-request@email.rutgers.edu](mailto:ECE_SIP-request@email.rutgers.edu) with SUBSCRIBE to sign up for announcements.
* [Statistics Seminar](https://statistics.rutgers.edu/news-events/seminars): use the [signup form](https://lists.sas.rutgers.edu/mailman/listinfo/stat-seminar).
* [MSIS Seminar](https://msis-rutgers.net/seminar/): email [Prof. Jalaj Upadhyay](mailto:jalaj.upadhyay@rutgers.edu) to subscribe.
* [CS Theory Seminar](https://theory.cs.rutgers.edu/theory_seminar): there is a calendar feed/link you can add to your calendar.
* [DIMACS Weekly Announcements](http://dimacs.rutgers.edu/events/list): email [Linda Casals](mailto:lindac@dimacs.rutgers.edu) to get on the weekly announcement list.