# Resources on how to research

##### an idiosyncratic take by Anand D. Sarwate

These are some notes and miscellaneous items about skills and tips for success in research (and beyond!). It is (a) not comprehensive and (b) very idiosyncratic. It will hopefully get more organized and filled in over time. Rather than a monolithic document, things will be split up into subfiles for easier consumption.
 
Things I'm not going to talk about:

* what classes to take
* how to pass various exams

This is very much a work in progress and some stuff has yet to be written. If you have some suggestions, please email me.

## Some writeups

* [Professionalization](ProfessionalStuff.md): getting started and getting your presence as a researcher
* [Tools and software](Tools.md): useful tools for your research
* [Building a research diet](ResearchInputs.md): how to expand your horizons and dig deeper
* [Organizing your research](OrganizingWork.md): how to track and maintain progress. Here are two forms for pre-meeting and post-meetings to help keep research progress on track.
   * [Pre-meeting form](PreMtgForm.md)
   * [Post-meeting form](PostMtgForm.md)
* [Reading papers and literature search](ReadingSkills.md): the many ways to read a paper, how to do a literature search, and so on
* [Reviewing papers](Reviewing.md): relatedly, how to do peer review
* [Running computational experiments](Experiments.md): still a work in progress

## External resources

* [MIT Comm Lab's CommKit](https://mitcommlab.mit.edu/nse/use-the-commkit/)
* [Improving your academic writing: My top 10 tips](http://www.raulpacheco.org/2013/02/improving-your-academic-writing-my-top-10-tips/)

## Stuff to be added/written still

* Research skills
  * how to read a paper
  * doing an exhaustive literature search
  * how to think about experiments / simulations (to finish)
  * How to review a paper
* Communication skills
  * assessing the audience
  * "chalk" talks
  * poster design
  * giving a talk with slides
  * doing a technical writeup / anatomy of a paper
  * "pitching" a new project
* Software: (some is done already)
  * Python / ipython
  * visualization / graphing
  * git
  * LaTeX
  * bibliographic management
* all the writing stuff
  * notes
  * papers
  * theses
